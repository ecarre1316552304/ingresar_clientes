const id_client = document.getElementById("id_cliente")
const apellido = document.getElementById("apellido")
const nombre = document.getElementById("name")
const email = document.getElementById("email")
const telefono = document.getElementById("fono")
const form = document.getElementById("form")
const parrafo = document.getElementById("warnings")

form.addEventListener("submit", e=>{
    e.preventDefault()
    let warnings = "" 
    let entrar = false
    let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    if(id_client.value.length <1){
        warnings += `Por favor ingrese un ID de cliente <br>`
        entrar = true
    }
    if(apellido.value.length <5){
        warnings += `El apellido ingresado no es valido <br>`
        entrar = true
    }
    if(nombre.value.length <3){
        warnings += `El nombre ingresado no es valido <br>`
        entrar = true
    }
    if(!regexEmail.test(email.value)){
        warnings += `El email ingresado no es valido <br>`
        entrar = true
    }
    if(telefono.value.length <10){
        warnings += `El teléfono ingresado no es valido <br>`
        entrar = true
    }
    if(entrar){
        parrafo.innerHTML = warnings
    }else{
        parrafo.innerHTML = "Datos guardados con éxito"
    }
})